# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 12:43:24 2021

@author: Simona
"""

if __name__=="__main__":
    nome="Simona"
    eta=23
    anno="24/04/1998"
  #  print("something to print")
   # print("something to print in the next line")
    #print(" %.3s %d %.3f" %("ciao", 145, 12.5))
    print("My name is %s and I'm %d years old, I was born the %s" %(nome,eta,anno))
    print(f"My name is {nome} and I'm {eta} years old, I was born the {anno}")
    print(f" slash: 11/2= {11/2}")
    print(f"double slash: 11//2= {11//2}")
    print(f"asterisk:10*2 {10*2}")
    print(f"percent:11%2 {11%2} è il resto della divisione")
    print(f"less than:10<2 {10<2}")
    print(f"greater-equal than:10>=2 {10>=2}") 
    number=input("Please write a number")
    print("the number is %s" %(number))
    